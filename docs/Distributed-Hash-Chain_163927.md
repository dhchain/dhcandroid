
Promotion of DHC
----------------

How does it work? A person who already participating in promotion can invite a friend to join his/her team. In order to do that an invitee would need to acquire small amount of DHC coins. By default this amount is one coin but it can be changed to become bigger or smaller. This will affect reward distribution. A person contributing two coins will receive double reward than a person contributing one coin.

Let�s assume an invitee already downloaded and installed DHC Android application using instructions in [https://bitbucket.org/dhchain/dhcandroid/src/master/](https://bitbucket.org/dhchain/dhcandroid/src/master/) . Once installed and login into the app, the invitee can see his unique generated DHC address:

![](163927/163987.png)

The invitee will share the DHC address with the inviter who will send some amount to that address. After that the invitee will select menu Promote/Join Team:

![](163927/65576.png)

After confirming the passphrase the invitee will paste the address of the inviter and click on Join Team button:

![](163927/33136.png)

After few minutes invitation transactions will be confirmed and the inviter will see increase in the size of his/her team:

![](163927/33142.png)

The donation will be distributed up to ten levels among the chain of inviters corresponding to their contributions. After that the invitee becomes an inviter and can bring more friends to join promotion of the DHC network. The team can have maximum ten levels but will grow very quickly. If each inviter invites five people then by ten level the team can contain more than ten million participants and the original inviter can receive more than one million coins.

