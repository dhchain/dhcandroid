
Promoción de DHC
----------------

¿Como funciona? Una persona que ya esté participando en la promoción puede invitar a un amigo a unirse a su equipo. Para hacer eso, un invitado necesitaría adquirir una pequeña cantidad de monedas DHC. De forma predeterminada, esta cantidad es una moneda, pero se puede cambiar para hacerla más grande o más pequeña. Esto afectará la distribución de recompensas. Una persona que contribuya con dos monedas recibirá una recompensa doble que una persona que contribuya con una moneda.

Supongamos que un invitado ya descargó e instaló la aplicación DHC para Android siguiendo las instrucciones de [https://bitbucket.org/dhchain/dhcandroid/src/master/](https://bitbucket.org/dhchain/dhcandroid/src/master/) . Una vez instalada e iniciada la sesión en la aplicación, el invitado puede ver su dirección DHC única generada:

![](131098/33171.png)

El invitado compartirá la dirección de DHC con el invitado que enviará una cantidad a esa dirección. Después de eso, el invitado seleccionará el menú Promover / Unirse al equipo:

![](131098/164018.png)

Después de confirmar la contraseña, el invitado pegará la dirección del invitado y hará clic en el botón Unirse al equipo:

![](131098/196660.png)

Después de unos minutos, las transacciones de invitación serán confirmadas y el invitador verá un aumento en el tamaño de su equipo:

![](131098/33160.png)

La donación se distribuirá hasta diez niveles entre la cadena de invitados correspondiente a sus aportes. Después de eso, el invitado se convierte en invitador y puede traer más amigos para unirse a la promoción de la red DHC. El equipo puede tener un máximo de diez niveles, pero crecerá muy rápidamente. Si cada invitado invita a cinco personas, entonces por diez niveles el equipo puede contener más de diez millones de participantes y el invitado original puede recibir más de un millón de monedas.