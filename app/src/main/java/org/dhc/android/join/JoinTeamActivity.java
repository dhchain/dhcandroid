package org.dhc.android.join;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.util.AndroidConstants;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.MyPasswordTransformationMethod;
import org.dhc.android.util.PasswordUtil;
import org.dhc.android.R;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.gui.promote.JoinInfo;
import org.dhc.gui.promote.JoinInfoItem;
import org.dhc.gui.promote.JoinTransactionEvent;
import org.dhc.gui.promote.JoinTransactionEventListener;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.util.Coin;
import org.dhc.util.Constants;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Listeners;
import org.dhc.util.StringUtil;
import org.dhc.util.ThreadExecutor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JoinTeamActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    String address;
    String amount;

    String result = "";
    private PasswordUtil passwordUtil = new PasswordUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_message);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.VISIBLE);
        final TextView message = findViewById(R.id.message);
        message.setText(getString(R.string.wait));

        new AndroidExecutor() {
            Set<Transaction> transactions;
            Coin balance;

            @Override
            public void start() {
                transactions =  DhcLiteHelper.getTransactionsForApp();
                balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
            }

            @Override
            public void finish() {
                final ProgressBar pgsBar = findViewById(R.id.progressBar);
                pgsBar.setVisibility(View.INVISIBLE);
                if(transactions != null && !transactions.isEmpty()) {
                    String str = getString(R.string.you_already_joined_team);
                    updateStatus(str);
                    Toast.makeText(JoinTeamActivity.this, str, Toast.LENGTH_LONG).show();
                    final TextView message = findViewById(R.id.message);
                    message.setText(str);
                    return;
                }

                if(Coin.ZERO.equals(balance)) {
                    String str = getString(R.string.balance_is_zero);
                    updateStatus(str);
                    Toast.makeText(JoinTeamActivity.this, str, Toast.LENGTH_LONG).show();
                    final TextView message = findViewById(R.id.message);
                    message.setText(str);
                    return;
                }

                showLoginForm();
            }
        }.run();

    }

    private void showLoginForm() {
        setContentView(R.layout.activity_main);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        Button submitButton = findViewById(R.id.button);
        final EditText password = findViewById(R.id.password);
        password.setTransformationMethod(new MyPasswordTransformationMethod());

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(verifyPassphrase(v, password.getText().toString().trim(), pgsBar)) {
                        showJoinTeamForm();
                    } else {
                        showLoginForm();
                    }
                }
                return false;
            }
        });

        submitButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(verifyPassphrase(v, password.getText().toString().trim(), pgsBar)) {
                    showJoinTeamForm();
                } else {
                    showLoginForm();
                }
            }
        });
    }

    private boolean verifyPassphrase(View v, String password, ProgressBar pgsBar) {
        hideKeyboard(v);
        boolean result = isPassphraseValid(password);
        if(result) {
            pgsBar.setVisibility(View.VISIBLE);
        }
        return result;
    }

    private boolean isPassphraseValid(String passphrase) {

        try {
            if (!passwordUtil.loadKeys(passphrase)) {
                logger.error(getResources().getString(R.string.no_load_key_file));
                File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
                if(keyFile.exists()) {
                    Toast.makeText(this, getResources().getString(R.string.wrong_password), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    private void showJoinTeamForm() {
        setContentView(R.layout.activity_join_team);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        Button sendTransactionButton = findViewById(R.id.joinTeamButton);
        sendTransactionButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        sendTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                load();
                if(!validate()) {
                    showAlert();
                    return;
                }
                if(DhcAddress.getMyDhcAddress().equals(new DhcAddress(address))) {
                    Toast.makeText(JoinTeamActivity.this, getResources().getString(R.string.cannot_use_your_own_address), Toast.LENGTH_LONG).show();
                    return;
                }
                setContentView(R.layout.activity_send_transaction_wait);
                process();
            }
        });
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {

        Set<Transaction> transactions =  DhcLiteHelper.getTransactionsForApp();
        if(transactions != null && !transactions.isEmpty()) {
            new Handler(Looper.getMainLooper()).post(new Runnable(){
                @Override
                public void run() {
                    updateStatus(getString(R.string.you_already_joined_team));
                    Toast.makeText(JoinTeamActivity.this, getResources().getString(R.string.you_already_joined_team), Toast.LENGTH_LONG).show();
                }
            });
            return;
        }

        final Coin amount = Coin.ONE.multiply(Double.parseDouble(this.amount));

        Coin balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
        if(balance.less(amount)) {
            new Handler(Looper.getMainLooper()).post(new Runnable(){
                @Override
                public void run() {
                    updateStatus(getString(R.string.balance_less_than_amount));
                    Toast.makeText(JoinTeamActivity.this, getResources().getString(R.string.balance_less_than_amount), Toast.LENGTH_LONG).show();
                }
            });

            return;
        }

        JoinInfo joinInfo = DhcLiteHelper.getJoinInfo(new DhcAddress(address), amount);
        if(joinInfo == null) {
            new Handler(Looper.getMainLooper()).post(new Runnable(){
                @Override
                public void run() {
                    updateStatus(getResources().getString(R.string.could_not_retrieve_joinInfo_for) + " " + address);
                    Toast.makeText(JoinTeamActivity.this, getResources().getString(R.string.could_not_retrieve_joinInfo_for) + " " + address, Toast.LENGTH_LONG).show();
                }
            });
            return;
        }

        split(joinInfo, amount);

        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        result = getResources().getString(R.string.joined) + " " + address + " " + getResources().getString(R.string.for_word) + " " +
                amount.toNumberOfCoins() + " " + getResources().getString(R.string.coins);
    }

    private void split(JoinInfo joinInfo, Coin amount) {
        if(!joinInfo.isValid()) {
            logger.info("joinInfo.isValid() returned false");
            return;
        }
        try {
            Map<Integer, JoinInfoItem> items = joinInfo.getItems();
            List<Coin> list = new ArrayList<>();
            list.add(joinInfo.getFraction(amount, joinInfo.getAmount()));
            for(JoinInfoItem item: items.values()) {
                list.add(joinInfo.getFraction(amount, item.getAmount()));
            }
            Transaction transaction = new Transaction();
            Set<TransactionOutput> outputs = DhcLiteHelper.getTransactionOutputs();
            transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, outputs, list.toArray(new Coin[0]));
            Listeners.getInstance().addEventListener(JoinTransactionEvent.class, new JoinTransactionEventListener(amount, transaction, joinInfo));
            Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
            Listeners.getInstance().sendEvent(new JoinTransactionEvent(transaction));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void processSync() {
        setContentView(R.layout.activity_join_team_complete);
        TextView resultTextView = findViewById(R.id.resultTextView);
        resultTextView.setText(result);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private void showAlert() {
        Toast.makeText(this, getResources().getString(R.string.entered_info_not_correct), Toast.LENGTH_LONG).show();
    }

    private boolean validate() {
        if(!CryptoUtil.isDhcAddressValid(address)) {
            return false;
        }
        try {
            Double.parseDouble(amount);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private void load() {
        EditText dhcAddressEditText = findViewById(R.id.dhcAddressEditText);
        address = StringUtil.trim(dhcAddressEditText.getText().toString());
        EditText amountInCoinsEditText = findViewById(R.id.amountInCoinsEditText);
        amount = StringUtil.trim(amountInCoinsEditText.getText().toString());
    }
}
