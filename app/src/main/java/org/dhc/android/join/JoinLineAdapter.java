package org.dhc.android.join;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.dhc.android.R;
import org.dhc.blockchain.Transaction;
import org.dhc.gui.promote.JoinLine;
import org.dhc.util.Coin;
import org.dhc.util.DhcLogger;

import java.util.Date;
import java.util.List;

public class JoinLineAdapter extends ArrayAdapter<JoinLine> {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private final Activity context;
    private final List<JoinLine> list;

    static class ViewHolder {
        public TextView position;
        public TextView count;
        public TextView amount;
    }

    public JoinLineAdapter(Activity context, List<JoinLine> list) {
        super(context, R.layout.joinline_rowlayout, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.joinline_rowlayout, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.position = (TextView) rowView.findViewById(R.id.position);
            viewHolder.count = (TextView) rowView.findViewById(R.id.count);
            viewHolder.amount = (TextView) rowView.findViewById(R.id.amount);

            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        JoinLine line = list.get(position);

        holder.position.setText(line.getPosition());
        holder.count.setText(line.getCount());
        holder.amount.setText(line.getAmount());

        return rowView;
    }

}
