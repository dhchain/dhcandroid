package org.dhc.android.join;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;
import org.dhc.blockchain.Transaction;
import org.dhc.gui.promote.JoinLine;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.PeerSync;
import org.dhc.util.Coin;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ViewTeamActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result;
    private Set<JoinLine> set;
    private Set<Transaction> transactions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_transactions_wait);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() throws Exception {
                processAsync();
            }

            @Override
            public void finish() throws Exception {
                processSync();
            }
        }.run();
    }

    private void processAsync() {

        set = DhcLiteHelper.getJoinLines();
        if(set == null) {
            result = getString(R.string.no_teams) + " " + DhcAddress.getMyDhcAddress();
            logger.info(result);
            PeerSync.getInstance().executeNow();
            return;
        }
        result = getString(R.string.number_of_teams) + " " + set.size();
        logger.info(result);
        transactions =  DhcLiteHelper.getTransactionsForApp();
    }

    private void processSync() {
        setContentView(R.layout.activity_view_team_complete);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        if(set == null || set.isEmpty()) {
            setContentView(R.layout.activity_view_team_empty);
            updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
            final TextView message = findViewById(R.id.message);
            if(transactions == null || transactions.isEmpty()) {
                String str = getString(R.string.you_not_joined_team);
                message.setText(str);
            } else {
                String str = getString(R.string.you_joined_team);
                message.setText(str);
            }
            return;
        }
        ListView listView = findViewById(R.id.simpleListView);

        List<JoinLine> list = new ArrayList<>();

        int totalCount = 0;
        Coin totalAmount = Coin.ZERO;
        for(JoinLine line: set) {
            totalCount = totalCount + Integer.parseInt(line.getCount());
            totalAmount = totalAmount.add(new Coin(Long.parseLong(line.getAmount())));
        }
        String totalTitle = getResources().getString(R.string.total);
        JoinLine totalLine = new JoinLine(totalTitle, Integer.toString(totalCount), Long.toString(totalAmount.getValue()));
        set.add(totalLine);

        int counter = 1;
        for(JoinLine line: set) {
            String position = line.getPosition().equals(totalTitle)? totalTitle: Integer.toString(counter++);
            list.add(new JoinLine(position, line.getCount(), new Coin(Long.parseLong(line.getAmount())).toNumberOfCoins()));
        }

        JoinLine titleLine = new JoinLine(getResources().getString(R.string.position) ,
                getResources().getString(R.string.count), getResources().getString(R.string.amount) + "(" + getResources().getString(R.string.coins) + ")" );
        list.add(0, titleLine);

        JoinLineAdapter arrayAdapter = new JoinLineAdapter(this, list);
        listView.setAdapter(arrayAdapter);

    }

}
