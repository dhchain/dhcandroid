package org.dhc.android.util;

public interface AndroidConstants {

    public static final int MAX_NUMBER_OF_KEYWORDS = 10;
    public static final String KEYS = "config/key.csv";
    public static final String DHC = "DHC";

}
