package org.dhc.android.util;

import android.os.Handler;
import android.os.Looper;

import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;

public abstract class AndroidExecutor {

    private static final DhcLogger logger = DhcLogger.getLogger();

    public abstract void start() throws Exception;
    public abstract void finish() throws Exception;
    public void run() {
        ThreadExecutor.getInstance().execute(new DhcRunnable("Android Executor") {
            public void doRun() {
                try {
                    start();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                new Handler(Looper.getMainLooper()).post(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            finish();
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                });
            }
        });
    }

}
