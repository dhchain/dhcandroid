package org.dhc.android.util;

import org.dhc.android.MainActivity;
import org.dhc.network.Network;
import org.dhc.network.PeerSync;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;

public class StartNetwork {

	private static final DhcLogger logger = DhcLogger.getLogger();
	private static final StartNetwork instance = new StartNetwork();
	public static StartNetwork getInstance() {
		return instance;
	}
	
	public void start(final MainActivity mainActivity) {
		ThreadExecutor.getInstance().execute(new DhcRunnable("StartNetwork") {
			public void doRun() {
				Network network = Network.getInstance();
				network.setPort(0);
				network.start();
				PeerSync.getInstance().start();
				mainActivity.connectedToDhcNetwork();
			}
		});
	}

}
