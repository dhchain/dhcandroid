package org.dhc.android.util;

import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.network.PeerSync;
import org.dhc.util.DhcLogger;

import java.util.HashSet;
import java.util.Set;

public class DHCAndroidUtil {

    private static final DhcLogger logger = DhcLogger.getLogger();

    public static final String STOP = "stop";
    public static final String START = "start";

    public static void enableScroll(final EditText editText) {

        editText.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (editText.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

    }

    public static Set<String> getKeywords(String keywords) {
        Set<String> set = new HashSet<>();
        String[] strings = keywords.split(" ");
        for(String str: strings) {
            if(set.size() > AndroidConstants.MAX_NUMBER_OF_KEYWORDS) {
                break;
            }
            if(!"".equals(str)) {
                set.add(str.toLowerCase());
            }
        }
        return set;
    }

    public static String getNetworkStatus() {
        Network network = Network.getInstance();
        return "" + network.getMyBucketPeers().size() + ", " + network.getPower() + ", " + network.getBucketKey();
    }

    public static void checkNetwork() {
        DhcLiteHelper.checkNetwork();
    }
}
