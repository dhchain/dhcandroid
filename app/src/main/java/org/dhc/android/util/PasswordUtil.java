package org.dhc.android.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcLogger;
import org.dhc.util.Wallet;
import org.dhc.util.Base58;
import org.dhc.util.Constants;
import org.dhc.util.Encryptor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;

public class PasswordUtil {

	private static final DhcLogger logger = DhcLogger.getLogger();
	
	static {
		setupBouncyCastle();
	}

	private static final Encryptor encryptor = new Encryptor();

	public static void setupBouncyCastle() {
		Security.setProperty("crypto.policy", "unlimited");
		Security.addProvider(new BouncyCastleProvider());
		final Provider provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
		if (provider == null) {
			// Web3j will set up the provider lazily when it's first used.
			return;
		}
		if (provider.getClass().equals(BouncyCastleProvider.class)) {
			// BC with same package name, shouldn't happen in real life.
			return;
		}
		// Android registers its own BC provider. As it might be outdated and might not include
		// all needed ciphers, we substitute it with a known BC bundled in the app.
		// Android's BC has its package rewritten to "com.android.org.bouncycastle" and because
		// of that it's possible to have another BC implementation loaded in VM.
		Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
		Security.insertProviderAt(new BouncyCastleProvider(), 1);
	}

	public PasswordUtil() {

	}
	
	public boolean loadKeys(String passphrase) {
		File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
		try {
			try (
					InputStream is = new FileInputStream(keyFile);
					BufferedReader in = new BufferedReader(new InputStreamReader(is));
			) {
				String line;
				while ((line = in.readLine()) != null) {
					String[] strs = line.split(",");
					if(strs.length != 2) {
						continue;
					}
					Wallet wallet = Wallet.getInstance();
					PublicKey publicKey = CryptoUtil.loadPublicKey(strs[0]);
					PrivateKey privateKey = CryptoUtil.loadPrivateKey(encryptor.decrypt(passphrase, Base58.decode(strs[1])));
					if(privateKey == null) {
						System.out.println("Passphrase entered was not correct. Try again.");
						return false;
					}
					wallet.setPrivateKey(privateKey);
					wallet.setPublicKey(publicKey);
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return true;
	}

	public void saveKey(Wallet wallet, String passphrase) {
		File targetFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
		File parent = targetFile.getParentFile();
		if (!parent.exists() && !parent.mkdirs()) {
			throw new IllegalStateException("Couldn't create dir: " + parent);
		}
		try {
			try (
					OutputStream os = new FileOutputStream(targetFile, false);
					BufferedWriter out = new BufferedWriter(new OutputStreamWriter(os));
			) {
				String publicKey = Base58.encode(wallet.getPublicKey().getEncoded());
				String privateKey = Base58.encode(encryptor.encrypt(passphrase, wallet.getPrivateKey().getEncoded()));
				out.write(publicKey + "," + privateKey);
				out.newLine();
				out.flush();
				System.out.println("Encrypted key were saved to " + targetFile.getAbsolutePath());
				System.out.println("Backup " + targetFile.getAbsolutePath() + " to a safe location");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void generateKey(String passPhrase) {
		Wallet wallet = Wallet.getInstance();
		wallet.generateKeyPair();
		saveKey(wallet, passPhrase);
	}

}
