package org.dhc.android;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import org.dhc.android.file.StartLogViewerActivity;
import org.dhc.android.file.StartShowPeersActivity;
import org.dhc.util.DhcLogger;

public class StartActivity extends AppCompatActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.start_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {

            case R.id.view_log:
                intent = new Intent(this, StartLogViewerActivity.class);
                startActivity(intent);
                return true;

            case R.id.show_peers:
                intent = new Intent(this, StartShowPeersActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
