package org.dhc.android.persistance;

public class Address {

    private String dhcAddress;
    private String name;

    public Address(String dhcAddress, String name) {
        this.dhcAddress = dhcAddress;
        this.name = name;
    }

    public String getDhcAddress() {
        return dhcAddress;
    }

    public String getName() {
        return name;
    }

}
