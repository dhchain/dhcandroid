package org.dhc.android.coins;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.persistance.AddressStore;
import org.dhc.blockchain.Keywords;
import org.dhc.blockchain.Transaction;
import org.dhc.util.Constants;
import org.dhc.util.DhcLogger;

import java.util.Date;

public class ShowTransactionActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private AddressStore addressStore;
    private Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logger.info("ShowTransactionActivity START");
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_show_transaction);
            transaction = (Transaction) getIntent().getSerializableExtra("transaction");
            addressStore = new AddressStore(getApplicationContext());
            logger.info("ShowTransactionActivity got transaction {}", transaction);
            process();
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }

    private void process() {
        Button buttonSender = findViewById(R.id.sender);
        final String senderDhcAddress = transaction.getSenderDhcAddress().toString();
        String name = addressStore.findNameByDhcAddress(senderDhcAddress);
        if(name == null) {
            buttonSender.setText(senderDhcAddress);
        } else {
            buttonSender.setText(name);
        }

        buttonSender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressClicked(senderDhcAddress);
            }
        });

        Button buttonReceiver = findViewById(R.id.recipient);
        final String recipientDhcAddress = transaction.getReceiver().toString();
        name = addressStore.findNameByDhcAddress(recipientDhcAddress);
        if(name == null) {
            buttonReceiver.setText(recipientDhcAddress);
        } else {
            buttonReceiver.setText(name);
        }

        buttonReceiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressClicked(recipientDhcAddress);
            }
        });

        TextView value = findViewById(R.id.value);
        value.setText(transaction.getValue().toNumberOfCoins() + " " + getResources().getString(R.string.coins));
        TextView fee = findViewById(R.id.fee);
        fee.setText(transaction.getFee().toNumberOfCoins() + " " + getResources().getString(R.string.coins));
        TextView date = findViewById(R.id.date);
        date.setText(new Date(transaction.getTimeStamp()).toString());
        TextView data = findViewById(R.id.data);
        data.setText(transaction.getExpiringData()!=null? transaction.getExpiringData().getData():"");
        TextView application = findViewById(R.id.application);
        application.setText(transaction.getApp());
        long validFor = transaction.getExpiringData() == null ? Constants.MINUTES_IN_A_YEAR: transaction.getExpiringData().getValidForNumberOfBlocks();
        String expire = new Date(transaction.getTimeStamp() + validFor * 60000).toString();
        TextView expireTextView = findViewById(R.id.expire);
        expireTextView.setText(expire);
        TextView keywordsTextView = findViewById(R.id.keywords);
        Keywords keywords = transaction.getKeywords();
        if(keywords == null) {
            keywordsTextView.setText("");
        } else {
            keywordsTextView.setText(keywords.toString());
        }


    }

    private void addressClicked(String dhcAddress) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("dhcAddress", dhcAddress);
        intent.putExtra("transaction", transaction);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }


}
