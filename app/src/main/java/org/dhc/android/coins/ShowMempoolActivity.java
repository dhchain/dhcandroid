package org.dhc.android.coins;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.dhc.android.persistance.AddressStore;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;
import org.dhc.blockchain.Transaction;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.PeerSync;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ShowMempoolActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result;
    private List<Transaction> list;
    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_transactions_wait);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        addressStore = new AddressStore(getApplicationContext());

        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() throws Exception {
                processAsync();
            }

            @Override
            public void finish() throws Exception {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        Set<Transaction> set = DhcLiteHelper.getMempoolTransactions();
        if(set == null) {
            result = "Could not retrieve transactions for" + getResources().getString(R.string.could_not_retrieve_transactions_for) + " " + DhcAddress.getMyDhcAddress();
            logger.info(result);
            PeerSync.getInstance().executeNow();
            return;
        }
        result = getResources().getString(R.string.number_of_transactions_retrieved) + " " + set.size();
        logger.info(result);
        list = new ArrayList<Transaction>(set);
    }

    private void processSync() {
        setContentView(R.layout.activity_show_transactions_complete);
        final SwipeRefreshLayout swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        list = null;
                        process();
                    }
                }
        );
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        if(list == null || list.isEmpty()) {
            setContentView(R.layout.activity_show_transactions_empty);
            updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
            return;
        }
        ListView listView = findViewById(R.id.simpleListView);
        TransactionAdapter arrayAdapter = new TransactionAdapter(this, list, addressStore);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transaction transaction = (Transaction) parent.getItemAtPosition(position);
                show(transaction);
            }
        });
    }

    private void show(Transaction transaction) {
        Intent intent = new Intent(this, ShowTransactionActivity.class);
        intent.putExtra("transaction", transaction);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }

}
