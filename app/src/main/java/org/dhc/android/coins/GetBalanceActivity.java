package org.dhc.android.coins;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.PeerSync;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;

public class GetBalanceActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_balance);
        Button getBalanceButton = findViewById(R.id.get_balance_button);
        final EditText addressEditText = findViewById(R.id.editText);

        addressEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        addressEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    hideKeyboard(v);
                    buttonClicked();
                }
                return false;
            }
        });

        getBalanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked();
            }
        });
        DhcLiteHelper.checkNetwork();
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private void buttonClicked() {
        final EditText addressEditText = findViewById(R.id.editText);
        final String dhcAddress = addressEditText.getText().toString().trim();
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.VISIBLE);
        Toast.makeText(this, getResources().getString(R.string.get_balance_wait), Toast.LENGTH_LONG).show();

        new AndroidExecutor() {
            private String balance;
            @Override
            public void start() throws Exception {
                balance = DhcLiteHelper.getBalance(new DhcAddress(dhcAddress)).toNumberOfCoins();
            }

            @Override
            public void finish() throws Exception {
                complete(balance, dhcAddress);
            }
        }.run();
    }

    private void complete(final String balance, final String dhcAddress) {
        setContentView(R.layout.activity_show_balance);
        final TextView addressTextView = findViewById(R.id.dhc_address_textView);
        addressTextView.setText(dhcAddress);
        if(balance == null) {
            updateStatus(getResources().getString(R.string.fail_retrieve_balance));
            Toast.makeText(this, getResources().getString(R.string.fail_retrieve_balance), Toast.LENGTH_LONG).show();
            PeerSync.getInstance().executeNow();
            return;
        }
        final TextView balanceTextView = findViewById(R.id.balance_textView);
        balanceTextView.setText(balance + " " + getResources().getString(R.string.coins));
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

}
