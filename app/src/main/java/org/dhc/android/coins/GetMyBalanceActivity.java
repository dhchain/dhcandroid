package org.dhc.android.coins;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;

public class GetMyBalanceActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_balance);
        final TextView balance = findViewById(R.id.balance);
        balance.setText(getResources().getString(R.string.get_balance_wait));


        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.VISIBLE);
        toast = Toast.makeText(this, getResources().getString(R.string.get_balance_wait), Toast.LENGTH_LONG);
        toast.show();

        new AndroidExecutor() {
            private String balance;
            @Override
            public void start() throws Exception {
                balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress()).toNumberOfCoins();
            }

            @Override
            public void finish() throws Exception {
                complete(balance);
            }
        }.run();
    }



    private void complete(final String balance) {
        final TextView balanceTextView = findViewById(R.id.balance);
        if(balance == null) {
            balanceTextView.setText(getResources().getString(R.string.fail_retrieve_balance));
            Toast.makeText(this, getResources().getString(R.string.fail_retrieve_balance), Toast.LENGTH_LONG).show();
        } else {
            balanceTextView.setText(balance + " " + getResources().getString(R.string.coins));
        }
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.INVISIBLE);

        toast.cancel();

        final TextView address = findViewById(R.id.address);
        address.setText(DhcAddress.getMyDhcAddress().toString());
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

}
