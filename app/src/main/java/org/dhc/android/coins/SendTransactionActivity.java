package org.dhc.android.coins;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.persistance.AddressStore;
import org.dhc.android.util.AndroidConstants;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.MyPasswordTransformationMethod;
import org.dhc.android.util.PasswordUtil;
import org.dhc.android.R;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionData;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.util.Coin;
import org.dhc.util.Constants;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.io.File;
import java.util.Set;

public class SendTransactionActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String recipientDhcAddress;
    private String amount;
    private String fee;
    private String expire;
    private String expiringData;

    private String result = "";
    private PasswordUtil passwordUtil = new PasswordUtil();
    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressStore = new AddressStore(getApplicationContext());

        setContentView(R.layout.activity_show_message);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.VISIBLE);
        final TextView message = findViewById(R.id.message);
        message.setText(getString(R.string.wait));

        new AndroidExecutor() {
            Coin balance;

            @Override
            public void start() {
                balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
            }

            @Override
            public void finish() {
                final ProgressBar pgsBar = findViewById(R.id.progressBar);
                pgsBar.setVisibility(View.INVISIBLE);

                if(Coin.ZERO.equals(balance)) {
                    String str = getString(R.string.your_balance_is_zero);
                    updateStatus(str);
                    Toast.makeText(SendTransactionActivity.this, str, Toast.LENGTH_LONG).show();
                    final TextView message = findViewById(R.id.message);
                    message.setText(str);
                    return;
                }

                showLoginForm();
            }
        }.run();
    }

    private void showLoginForm() {
        setContentView(R.layout.activity_main);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        Button submitButton = findViewById(R.id.button);
        final EditText password = findViewById(R.id.password);
        password.setTransformationMethod(new MyPasswordTransformationMethod());

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(verifyPassphrase(v, password.getText().toString(), pgsBar)) {
                        showSendTransactionForm();
                    } else {
                        showLoginForm();
                    }
                }
                return false;
            }
        });

        submitButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(verifyPassphrase(v, password.getText().toString(), pgsBar)) {
                    showSendTransactionForm();
                } else {
                    showLoginForm();
                }
            }
        });
    }

    private boolean verifyPassphrase(View v, String password, ProgressBar pgsBar) {
        hideKeyboard(v);
        boolean result = isPassphraseValid(password);
        if(result) {
            pgsBar.setVisibility(View.VISIBLE);
        }
        return result;
    }

    private boolean isPassphraseValid(String passphrase) {

        try {
            if (!passwordUtil.loadKeys(passphrase)) {
                logger.error(getResources().getString(R.string.no_load_key_file));
                File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
                if(keyFile.exists()) {
                    Toast.makeText(this, getResources().getString(R.string.wrong_password), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    private void showSendTransactionForm() {
        setContentView(R.layout.activity_send_transaction);

        @SuppressLint("WrongViewCast")
        AutoCompleteTextView recipientDhcAddressEditText = findViewById(R.id.dhcAddressEditText);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, addressStore.getAllNames());
        recipientDhcAddressEditText.setAdapter(adapter);
        recipientDhcAddressEditText.setThreshold(1);

        DhcLiteHelper.checkNetwork();
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        Button sendTransactionButton = findViewById(R.id.sendTransactionButton);
        sendTransactionButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        sendTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                load();
                if(!validate()) {
                    showAlert();
                    return;
                }
                setContentView(R.layout.activity_send_transaction_wait);
                process();
            }
        });

        final EditText expiringDataEditText = findViewById(R.id.expiringDataEditText);
        final ScrollView scrollView = findViewById(R.id.scrollView);
        final TextView statusBar = findViewById(R.id.statusBar);
        expiringDataEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus){
                    scrollView.smoothScrollTo(0,statusBar.getBottom());
                }
            }
        });

        DHCAndroidUtil.enableScroll(expiringDataEditText);
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        final Coin amount = Coin.ONE.multiply(Double.parseDouble(this.amount));
        final Coin feeAmount = new Coin(Long.parseLong(fee));
        final TransactionData expiringData = this.expiringData == null? null: new TransactionData(this.expiringData, Long.parseLong(expire));

        Coin balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
        if(balance.less(amount.add(feeAmount))) {
            result = getString(R.string.balance_less_than_required);
            return;
        }

        Network network = Network.getInstance();
        Transaction transaction = new Transaction();

        Set<TransactionOutput> outputs = DhcLiteHelper.getTransactionOutputs();
        try {
            transaction.create(new DhcAddress(recipientDhcAddress), amount, feeAmount, expiringData, null, outputs);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result = e.getMessage();
            return;
        }

        network.sendToAllMyPeers(new SendTransactionMessage(transaction));
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());

        String recipientName = addressStore.findNameByDhcAddress(recipientDhcAddress);
        if(recipientName == null) {
            recipientName = recipientDhcAddress;
        }

        result = getResources().getString(R.string.success_sent) + " " + amount.toNumberOfCoins() + " " + getResources().getString(R.string.coins_to) + " " + recipientName;
    }

    private void processSync() {
        setContentView(R.layout.activity_send_transaction_complete);
        TextView resultTextView = findViewById(R.id.resultTextView);
        resultTextView.setText(result);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private void showAlert() {
        Toast.makeText(this, getResources().getString(R.string.entered_info_not_correct), Toast.LENGTH_LONG).show();
    }

    private boolean validate() {
        if(!CryptoUtil.isDhcAddressValid(recipientDhcAddress)) {
            return false;
        }
        try {
            Double.parseDouble(amount);
            Long.parseLong(fee);
            if(expiringData != null) {
                Long.parseLong(expire);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private void load() {
        EditText recipientDhcAddressEditText = findViewById(R.id.dhcAddressEditText);
        recipientDhcAddress = StringUtil.trim(recipientDhcAddressEditText.getText().toString());

        String str = addressStore.findDhcAddressByName(recipientDhcAddress);
        if(str != null) {
            recipientDhcAddress = str;
        }

        EditText amountInCoinsEditText = findViewById(R.id.amountInCoinsEditText);
        amount = StringUtil.trim(amountInCoinsEditText.getText().toString());
        EditText feeInSatoshisEditText = findViewById(R.id.feeInSatoshisEditText);
        fee = StringUtil.trim(feeInSatoshisEditText.getText().toString());
        EditText expireAfterBlocksEditText = findViewById(R.id.expireAfterBlocksEditText);
        expire = StringUtil.trimToNull(expireAfterBlocksEditText.getText().toString());
        expire = expire == null? "525600": expire;
        EditText expiringDataEditText = findViewById(R.id.expiringDataEditText);
        expiringData = StringUtil.trimToNull(expiringDataEditText.getText().toString());
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }

}
