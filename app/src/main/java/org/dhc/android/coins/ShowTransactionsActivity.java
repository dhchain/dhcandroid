package org.dhc.android.coins;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.dhc.android.persistance.AddressStore;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.BaseActivity;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;
import org.dhc.blockchain.Transaction;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.PeerSync;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ShowTransactionsActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result;
    private List<Transaction> list;
    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_show_transactions_wait);
            updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
            addressStore = new AddressStore(getApplicationContext());
            process();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() throws Exception {
                processAsync();
            }

            @Override
            public void finish() throws Exception {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        Set<Transaction> set = DhcLiteHelper.getTransactions();

        if(set == null) {
            result = "Could not retrieve transactions for " + DhcAddress.getMyDhcAddress();
            logger.info(result);
            PeerSync.getInstance().executeNow();
            return;
        }
        result = "Number of transactions retrieved " + set.size();
        logger.info(result);
        list = new ArrayList<Transaction>(set);
    }

    private void processSync() {
        setContentView(R.layout.activity_show_transactions_complete);
        final SwipeRefreshLayout swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        list = null;
                        process();
                    }
                }
        );
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        if(list == null || list.isEmpty()) {
            setContentView(R.layout.activity_show_transactions_empty);
            updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
            return;
        }
        ListView listView = findViewById(R.id.simpleListView);
        TransactionAdapter arrayAdapter = new TransactionAdapter(this, list, addressStore);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transaction transaction = (Transaction) parent.getItemAtPosition(position);
                logger.info("ShowTransactionsActivity click on transaction {}", transaction);
                show(transaction);
            }
        });
    }

    private void show(Transaction transaction) {
        try {
            Intent intent = new Intent(this, ShowTransactionActivity.class);
            intent.putExtra("transaction", transaction);
            startActivity(intent);
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }

}
