package org.dhc.android.coins;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.persistance.Address;
import org.dhc.android.persistance.AddressStore;
import org.dhc.blockchain.Transaction;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

public class AddAddressActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        addressStore = new AddressStore(getApplicationContext());
        final Transaction transaction = (Transaction)getIntent().getSerializableExtra("transaction");
        final String dhcAddress = (String)getIntent().getSerializableExtra("dhcAddress");
        TextView dhc_address = findViewById(R.id.dhc_address);
        dhc_address.setText(dhcAddress);

        Button button = findViewById(R.id.add_to_address_book);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked(dhcAddress, transaction);
            }
        });

        EditText name = findViewById(R.id.name);
        String str = addressStore.findNameByDhcAddress(dhcAddress);
        if(str != null) {
            name.setText(str);
        }

        Button delete_address_button = findViewById(R.id.delete_address_button);
        delete_address_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClicked(dhcAddress, transaction);
            }
        });
    }

    private void deleteButtonClicked(String dhcAddress, Transaction transaction) {
        TextView dhc_address = findViewById(R.id.dhc_address);

        addressStore.deleteByDhcAddress(dhcAddress);

        Intent intent = new Intent(this, ShowTransactionActivity.class);
        intent.putExtra("transaction", transaction);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.address_was_deleted), Toast.LENGTH_LONG).show();
    }

    private void buttonClicked(String dhcAddress, Transaction transaction) {
        EditText name = findViewById(R.id.name);
        String nameString = name.getText().toString();
        if(StringUtil.isEmpty(nameString)) {
            Toast.makeText(this, getResources().getString(R.string.name_cannot_be_blank), Toast.LENGTH_LONG).show();
            return;
        }
        try {
            addressStore.save(new Address(dhcAddress, name.getText().toString()));
        } catch (SQLiteConstraintException e) {
            Toast.makeText(this, getResources().getString(R.string.name_already_exists), Toast.LENGTH_LONG).show();
            return;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, ShowTransactionActivity.class);
        intent.putExtra("transaction", transaction);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.address_was_added), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }

}
