package org.dhc.android.coins;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.dhc.android.R;
import org.dhc.android.persistance.AddressStore;
import org.dhc.blockchain.Transaction;
import org.dhc.util.DhcLogger;

import java.util.Date;
import java.util.List;

public class TransactionAdapter extends ArrayAdapter<Transaction> {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private final Activity context;
    private final List<Transaction> list;
    private AddressStore addressStore;

    static class ViewHolder {
        public TextView sender;
        public TextView recipient;
        public TextView value;
        public TextView date;
    }

    public TransactionAdapter(Activity context, List<Transaction> list, AddressStore addressStore) {
        super(context, R.layout.transaction_rowlayout, list);
        this.context = context;
        this.list = list;
        this.addressStore = addressStore;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.transaction_rowlayout, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.value = (TextView) rowView.findViewById(R.id.value);
            viewHolder.sender = (TextView) rowView.findViewById(R.id.sender);
            viewHolder.recipient = (TextView) rowView.findViewById(R.id.recipient);
            viewHolder.date = (TextView) rowView.findViewById(R.id.date);

            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        Transaction transaction = list.get(position);

        String senderDhcAddress = transaction.getSenderDhcAddress().toString();
        String name = addressStore.findNameByDhcAddress(senderDhcAddress);
        if(name != null) {
            senderDhcAddress = name;
        }

        String receiver = transaction.getReceiver().toString();
        name = addressStore.findNameByDhcAddress(receiver);
        if(name != null) {
            receiver = name;
        }

        holder.value.setText(context.getResources().getString(R.string.amount) + ": " + transaction.getValue().toNumberOfCoins() + " coins");
        holder.sender.setText(context.getResources().getString(R.string.sender) + ": " + senderDhcAddress);
        holder.recipient.setText(context.getResources().getString(R.string.recipient) + ": " + receiver);
        holder.date.setText(context.getResources().getString(R.string.date) + ": " + new Date(transaction.getTimeStamp()).toString());

        return rowView;
    }

}
