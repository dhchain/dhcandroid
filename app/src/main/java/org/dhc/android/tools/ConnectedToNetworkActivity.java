package org.dhc.android.tools;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.messaging.NewMessageEventListener;
import org.dhc.android.messaging.NewMessageNotifier;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.lite.NewMessageEvent;
import org.dhc.lite.TransactionEvent;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Listeners;

public class ConnectedToNetworkActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private static final Listeners listeners = Listeners.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected_to_network);
        final TextView address = findViewById(R.id.address);
        address.setText(DhcAddress.getMyDhcAddress().toString());
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        listeners.addEventListener(NewMessageEvent.class, new NewMessageEventListener(getApplicationContext()));
        listeners.addEventListener(TransactionEvent.class, new TransactionEventListener(getApplicationContext()));
        startNotifier();
    }

    private void startNotifier() {
        Intent service = new Intent(this, NewMessageNotifier.class);
        service.setAction(DHCAndroidUtil.START);
        ContextCompat.startForegroundService(this, service);
    }



}
