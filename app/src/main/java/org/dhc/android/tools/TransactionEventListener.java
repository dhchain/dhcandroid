package org.dhc.android.tools;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import org.dhc.android.R;
import org.dhc.blockchain.Transaction;
import org.dhc.lite.TransactionEvent;
import org.dhc.util.DhcLogger;
import org.dhc.util.Event;
import org.dhc.util.EventListener;

public class TransactionEventListener implements EventListener {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private Context applicationContext;
    private MediaPlayer mp;

    public TransactionEventListener(Context applicationContext) {
        this.applicationContext = applicationContext;
        mp = MediaPlayer.create(applicationContext, R.raw.mined);
    }

    @Override
    public void onEvent(Event event) {
        TransactionEvent transactionEvent = (TransactionEvent)event;
        final Transaction transaction = transactionEvent.getTransaction();
        logger.info("Received new transaction event {}", transaction);
        new Handler(Looper.getMainLooper()).post(new Runnable(){
            @Override
            public void run() {
                try {
                    process(transaction);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });
    }

    private void process(Transaction transaction) {
        Toast.makeText(applicationContext, applicationContext.getResources().getString(R.string.transaction_was_accepted) +
                " " + transaction.getValue().toNumberOfCoins() + " " + applicationContext.getResources().getString(R.string.coins), Toast.LENGTH_SHORT).show();
        mp.start();
    }
}
