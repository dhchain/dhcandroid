package org.dhc.android.tools;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import org.dhc.android.BaseActivity;
import org.dhc.android.BuildConfig;
import org.dhc.android.MyPasswordTransformationMethod;
import org.dhc.android.R;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.util.PasswordUtil;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Wallet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class UpdateAppActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        setContentView(R.layout.activity_app_update);

        Button appUpdateButton = findViewById(R.id.appUpdateButton);

        appUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process();
            }
        });

    }

    private void process() {
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        pgsBar.setVisibility(View.VISIBLE);
        Toast.makeText(this, getResources().getString(R.string.wait), Toast.LENGTH_LONG).show();
        new AndroidExecutor() {
            private String balance;
            @Override
            public void start() throws Exception {
                download();
            }

            @Override
            public void finish() throws Exception {
                pgsBar.setVisibility(View.INVISIBLE);
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    File file = new File(getExternalCacheDir() + "/dhc-android.apk");
                    Uri apkUri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        apkUri = FileProvider.getUriForFile(UpdateAppActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", file);
                    } else {
                        apkUri = Uri.fromFile(file);
                    }
                    logger.info("apk URI: {}", UpdateAppActivity.this);
                    intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(UpdateAppActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    throw e;
                }

                setContentView(R.layout.activity_connected_to_network);
                final TextView address = findViewById(R.id.address);
                address.setText(DhcAddress.getMyDhcAddress().toString());
            }
        }.run();
    }

    private void download() {
        byte[] buffer = new byte[1024];
        int length;
        try (
             InputStream is = new URL("http://download.dhcne.org/drive/dhc-android.apk").openStream();
             DataInputStream dis = new DataInputStream(is);
             FileOutputStream fos = new FileOutputStream(new File(getExternalCacheDir() + "/dhc-android.apk"));
             ) {
             while ((length = dis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
             }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
