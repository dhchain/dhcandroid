package org.dhc.android.file;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import org.dhc.android.R;
import org.dhc.android.StartActivity;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.network.Network;
import org.dhc.network.Peer;
import org.dhc.util.Constants;
import org.dhc.util.ThreadExecutor;


import java.util.List;

public class StartShowPeersActivity extends StartActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private boolean active = true;
    private String text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_peers);
        active = true;
        ThreadExecutor.getInstance().execute(new DhcRunnable("NewMessageNotifier") {
            public void doRun() {
                process();
            }
        });
    }

    private void process() {
        final TextView peersTextView = findViewById(R.id.peers_text_view);
        while(active) {
            Network network = Network.getInstance();
            if(network == null) {
                ThreadExecutor.sleep(Constants.SECOND);
                continue;
            }
            final StringBuilder stringBuilder = new StringBuilder();
            List<Peer> list = network.getMyBucketPeers();
            for(Peer peer: list) {
                stringBuilder.append(peer.toString()).append(System.lineSeparator());
            }
            String localText = stringBuilder.toString();
            if(text.equals(localText)) {
                return;
            }
            text = localText;
            new Handler(Looper.getMainLooper()).post(new Runnable(){
                @Override
                public void run() {
                    try {
                        peersTextView.setText(text);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            });

            ThreadExecutor.sleep(Constants.SECOND);
        }
    }

    @Override
    protected void onDestroy() {
        active = false;
        super.onDestroy();
    }

}
