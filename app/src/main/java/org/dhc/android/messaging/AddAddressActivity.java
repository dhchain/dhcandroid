package org.dhc.android.messaging;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.persistance.Address;
import org.dhc.android.persistance.AddressStore;
import org.dhc.lite.SecureMessage;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

public class AddAddressActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        addressStore = new AddressStore(getApplicationContext());
        final SecureMessage secureMessage = (SecureMessage)getIntent().getSerializableExtra("secureMessage");
        TextView dhc_address = findViewById(R.id.dhc_address);

        String dhcAddress = secureMessage.getSenderDhcAddress();
        dhc_address.setText(dhcAddress);

        Button button = findViewById(R.id.add_to_address_book);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked(secureMessage);
            }
        });

        EditText name = findViewById(R.id.name);
        String str = addressStore.findNameByDhcAddress(dhcAddress);
        if(str != null) {
            name.setText(str);
        }

        Button delete_address_button = findViewById(R.id.delete_address_button);
        delete_address_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClicked(secureMessage);
            }
        });
    }

    private void deleteButtonClicked(SecureMessage secureMessage) {
        TextView dhc_address = findViewById(R.id.dhc_address);

        addressStore.deleteByDhcAddress(dhc_address.getText().toString());

        Intent intent = new Intent(this, ShowMessageActivity.class);
        intent.putExtra("secureMessage", secureMessage);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.address_was_deleted), Toast.LENGTH_LONG).show();
    }

    private void buttonClicked(SecureMessage secureMessage) {
        TextView dhc_address = findViewById(R.id.dhc_address);
        EditText name = findViewById(R.id.name);
        String nameString = name.getText().toString();
        if(StringUtil.isEmpty(nameString)) {
            Toast.makeText(this, getResources().getString(R.string.name_cannot_be_blank), Toast.LENGTH_LONG).show();
            return;
        }
        try {
            addressStore.save(new Address(dhc_address.getText().toString(), name.getText().toString()));
        } catch (SQLiteConstraintException e) {
            Toast.makeText(this, getResources().getString(R.string.name_already_exists), Toast.LENGTH_LONG).show();
            return;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, ShowMessageActivity.class);
        intent.putExtra("secureMessage", secureMessage);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.address_was_added), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
