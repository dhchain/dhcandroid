package org.dhc.android.messaging;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import org.dhc.android.util.AndroidConstants;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.util.Constants;
import org.dhc.util.DhcLogger;
import org.dhc.util.DhcRunnable;
import org.dhc.util.ThreadExecutor;
import org.dhc.android.util.PasswordUtil;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.android.R;

public class NewMessageNotifier extends Service {

    private static final DhcLogger logger = DhcLogger.getLogger();

    static {
        PasswordUtil.setupBouncyCastle();
    }

    private Context context;
    private String action;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logger.debug("onStartCommand intent.getAction()={}", intent.getAction());
        if (DHCAndroidUtil.STOP.equals(intent.getAction())) {
            action = DHCAndroidUtil.STOP;
            stopForeground(true);
            stopSelf();
        }
        context = getApplicationContext();
        Constants.FILES_DIRECTORY = getFilesDir().getAbsolutePath() + "/";
        run();

        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        logger.debug("onTaskRemoved");
        restart();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        logger.debug("onDestroy");
        super.onDestroy();
        restart();
    }

    private void restart() {
        if (DHCAndroidUtil.STOP.equals(action)) {
            return;
        }

        Intent restartServiceTask = new Intent(context,this.getClass());
        restartServiceTask.setAction(DHCAndroidUtil.START);
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent =PendingIntent.getService(context, 1,restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);
    }


    private void run() {

        startForeground();
        ThreadExecutor.getInstance().execute(new DhcRunnable("NewMessageNotifier") {
            public void doRun() {
                PowerManager.WakeLock wakeLock = null;
                try {
                    PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DHC::MyWakelockTag");
                    wakeLock.acquire();

                    while (!DHCAndroidUtil.STOP.equals(action)) {
                        try {
                            process();
                            ThreadExecutor.sleep(Constants.MINUTE * 5);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if(wakeLock != null) {
                        wakeLock.release();
                    }

                }
            }
        });
    }

    public void process() {
        DhcLiteHelper.checkNetwork();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = AndroidConstants.DHC;
            CharSequence name = AndroidConstants.DHC;
            String description = AndroidConstants.DHC;
            int importance = NotificationManager.IMPORTANCE_NONE;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startForeground() {
        logger.debug("startForeground");
        createNotificationChannel();
        Intent notificationIntent = new Intent(context, ShowMessagesActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, AndroidConstants.DHC);
        Notification notification = notificationBuilder
                .setOngoing(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(getResources().getString(R.string.new_messages_listener))
                .setPriority(NotificationCompat.PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                .build();
        startForeground(1, notification);
    }

}
