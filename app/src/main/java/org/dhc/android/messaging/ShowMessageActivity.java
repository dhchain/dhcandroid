package org.dhc.android.messaging;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.persistance.AddressStore;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.lite.SecureMessage;
import org.dhc.util.DhcLogger;
import org.dhc.util.Encryptor;
import org.dhc.util.Wallet;

import java.util.Date;

public class ShowMessageActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private static final Encryptor encryptor = new Encryptor();

    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressStore = new AddressStore(getApplicationContext());
        SecureMessage secureMessage = (SecureMessage) getIntent().getSerializableExtra("secureMessage");
        setContentView(R.layout.activity_show_secure_message);
        showMessage(secureMessage);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private void showMessage(final SecureMessage secureMessage) {
        String date = new Date(secureMessage.getTimeStamp()).toString();
        String expire = new Date(secureMessage.getTimeStamp() + secureMessage.getExpire() * 60000).toString();
        Wallet wallet = Wallet.getInstance();
        String subject = secureMessage.getSubject(wallet.getPrivateKey());
        Button sender = findViewById(R.id.sender);

        String senderDhcAddress = secureMessage.getSenderDhcAddress();
        String name = addressStore.findNameByDhcAddress(senderDhcAddress);
        if(name == null) {
            sender.setText(senderDhcAddress);
        } else {
            sender.setText(name);
        }

        sender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                senderClicked(secureMessage);
            }
        });

        TextView recipient = findViewById(R.id.recipient);

        String recipientName = addressStore.findNameByDhcAddress(secureMessage.getRecipient());
        if(recipientName == null) {
            recipientName = secureMessage.getRecipient();
        }

        recipient.setText(recipientName);

        TextView value = findViewById(R.id.value);
        value.setText(secureMessage.getValue().toNumberOfCoins());
        TextView fee = findViewById(R.id.fee);
        fee.setText(secureMessage.getFee().toNumberOfCoins());

        TextView dateTextView = findViewById(R.id.singleDateTextView);
        dateTextView.setText(date);

        TextView expireTextView = findViewById(R.id.expire);
        expireTextView.setText(expire);

        TextView subjectTextView = findViewById(R.id.singleSubjectTextView);
        subjectTextView.setText(subject);

        TextView body = findViewById(R.id.body);
        String bodyText = secureMessage.getBody(wallet.getPrivateKey());
        bodyText = bodyText == null? "": bodyText;
        body.setText(bodyText);

        Button reply = findViewById(R.id.reply);
        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked(secureMessage);
            }
        });
    }

    private void senderClicked(SecureMessage secureMessage) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra("secureMessage", secureMessage);
        startActivity(intent);
    }

    private void buttonClicked(SecureMessage secureMessage) {
        Intent intent = new Intent(this, SendMessageActivity.class);
        intent.putExtra("secureMessage", secureMessage);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }
}
