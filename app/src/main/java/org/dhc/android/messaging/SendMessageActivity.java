package org.dhc.android.messaging;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.persistance.AddressStore;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionData;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.SecureMessage;
import org.dhc.network.Network;
import org.dhc.util.Applications;
import org.dhc.util.Base58;
import org.dhc.util.Coin;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Encryptor;
import org.dhc.util.StringUtil;
import org.dhc.util.Wallet;


import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SendMessageActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private static final Encryptor encryptor = new Encryptor();

    private String recipientDhcAddress;
    private String fee;
    private String expire;
    private String subject;
    private String expiringData;
    String result = "";
    private AddressStore addressStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            doOnCreate(savedInstanceState);
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
        }
    }

    protected void doOnCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_send_message);
        addressStore = new AddressStore(getApplicationContext());

        AutoCompleteTextView recipientDhcAddressEditText = findViewById(R.id.recipientDhcAddressEditText);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, addressStore.getAllNames());
        recipientDhcAddressEditText.setAdapter(adapter);
        recipientDhcAddressEditText.setThreshold(1);

        SecureMessage secureMessage = (SecureMessage)getIntent().getSerializableExtra("secureMessage");
        if(secureMessage != null) {
            String recipientName = addressStore.findNameByDhcAddress(secureMessage.getSenderDhcAddress());
            if(recipientName == null) {
                recipientName = secureMessage.getSenderDhcAddress();
            }

            recipientDhcAddressEditText.setText(recipientName);
            EditText subjectEditText = findViewById(R.id.subjectEditText);
            Wallet wallet = Wallet.getInstance();
            subjectEditText.setText(getResources().getString(R.string.re) + " " + secureMessage.getSubject(wallet.getPrivateKey()));
        }
        String subject = (String)getIntent().getSerializableExtra("subject");
        if(subject != null) {
            String recipient = (String)getIntent().getSerializableExtra("recipient");
            String recipientName = addressStore.findNameByDhcAddress(recipient);
            if(recipientName == null) {
                recipientName = recipient;
            }

            recipientDhcAddressEditText.setText(recipientName);
            EditText subjectEditText = findViewById(R.id.subjectEditText);
            subjectEditText.setText(getResources().getString(R.string.re) + " " + subject);
        }

        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        Button sendMessageButton = findViewById(R.id.sendMessageButton);
        sendMessageButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    hideKeyboard(v);
                    load();
                    if (!validate()) {
                        showAlert();
                        return;
                    }
                    setContentView(R.layout.activity_send_message_wait);
                    process();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });

        Spinner expireAfterBlocksSpinner = findViewById(R.id.expireAfterBlocksSpinner);
        expireAfterBlocksSpinner.setSelection(5);
        expireAfterBlocksSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        expire = "10";
                        break;
                    case 1:
                        expire = "60";
                        break;
                    case 2:
                        expire = "1440";
                        break;
                    case 3:
                        expire = "10080";
                        break;
                    case 4:
                        expire = "43200";
                        break;
                    case 5:
                        expire = "525600";
                        break;

                    default:
                        expire = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                expire = null;
            }
        });

        final EditText expiringDataEditText = findViewById(R.id.expiringDataEditText);
        final ScrollView scrollView = findViewById(R.id.scrollView);
        final TextView statusBar = findViewById(R.id.statusBar);
        expiringDataEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus){
                    scrollView.smoothScrollTo(0,statusBar.getBottom());
                }
            }
        });

        DHCAndroidUtil.enableScroll(expiringDataEditText);

    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        logger.info("processAsync() start");
        Network network = Network.getInstance();
        final String dhcAddress = DhcAddress.getMyDhcAddress().toString();
        final Coin total = Coin.SATOSHI.add(new Coin(Long.parseLong(fee)));
        final Wallet wallet = Wallet.getInstance();
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());

        Coin balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
        if(balance.less(total)) {
            result = getString(R.string.balance_less_than_required);
            logger.info(result);
            return;
        }

        PublicKey recipient = DhcLiteHelper.getPublicKey(recipientDhcAddress);

        if(recipient == null) {
            logger.debug("Recipient public key is not found for DHC address {}", recipientDhcAddress);
            result = getString(R.string.key_not_found) + " " + recipientDhcAddress;
            return;
        }

        TransactionData transactionData = null;
        if(subject != null) {
            try {
                String str = subject + "\n" + expiringData;
                byte[] encrypted = encryptor.encryptAsymm(str.getBytes(StandardCharsets.UTF_8), recipient);
                str = Base58.encode(encrypted);
                transactionData = new TransactionData(str, Long.parseLong(expire));
                if(!str.equals(transactionData.getData())) {
                    logger.debug("Encrypted string length is {}", str.length());
                    result = getString(R.string.message_too_long);
                    return;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                result  = e.getMessage();
                return;
            }
        }

        Transaction transaction = new Transaction();
        transaction.setApp(Applications.MESSAGING);
        Set<TransactionOutput> outputs = DhcLiteHelper.getTransactionOutputs();
        try {
            transaction.create(CryptoUtil.getDhcAddressFromKey(recipient), Coin.SATOSHI, new Coin(Integer.parseInt(fee)), transactionData, null, outputs);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result = e.getMessage();
            return;
        }

        network.sendToAllMyPeers(new SendTransactionMessage(transaction));

        logger.info("sent {}", transaction);

        String recipientName = addressStore.findNameByDhcAddress(recipientDhcAddress);
        if(recipientName == null) {
            recipientName = recipientDhcAddress;
        }

        result = getResources().getString(R.string.successfully_sent_message) + " \"" + subject + "\" " +
                getResources().getString(R.string.to) + " " + recipientName;
    }

    private void processSync() {
        setContentView(R.layout.activity_send_message_complete);
        TextView resultTextView = findViewById(R.id.resultTextView);
        resultTextView.setText(result);
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private void showAlert() {
        Toast.makeText(this, getResources().getString(R.string.entered_info_not_correct), Toast.LENGTH_LONG).show();
    }

    private boolean validate() {
        if(!new DhcAddress(recipientDhcAddress).isDhcAddressValid()) {
            return false;
        }
        try {
            Long.parseLong(fee);
            if(subject != null) {
                Long.parseLong(expire);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private void load() {
        EditText recipientDhcAddressEditText = findViewById(R.id.recipientDhcAddressEditText);
        recipientDhcAddress = StringUtil.trim(recipientDhcAddressEditText.getText().toString());

        String str = addressStore.findDhcAddressByName(recipientDhcAddress);
        if(str != null) {
            recipientDhcAddress = str;
        }

        EditText feeInSatoshisEditText = findViewById(R.id.feeInSatoshisEditText);
        fee = StringUtil.trim(feeInSatoshisEditText.getText().toString());
        EditText subjectEditText = findViewById(R.id.subjectEditText);
        subject = StringUtil.trim(subjectEditText.getText().toString());
        EditText expiringDataEditText = findViewById(R.id.expiringDataEditText);
        expiringData = StringUtil.trim(expiringDataEditText.getText().toString());
    }

    @Override
    protected void onDestroy() {
        if(addressStore != null) {
            addressStore.onDestroy();
        }
        super.onDestroy();
    }
}
