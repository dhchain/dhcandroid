package org.dhc.android.posting;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.post.Rating;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.util.List;

public class MyRatingsActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result = "";
    private List<Rating> list;
    private String rater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            doOnCreate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void doOnCreate() {
        rater = (String)getIntent().getSerializableExtra("rater");
        setContentView(R.layout.activity_my_ratings_wait);
        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        findRatings();
    }

    private void findRatings() {
        if(rater == null) {
            rater  = DhcAddress.getMyDhcAddress().toString();
        }

        list = DhcLiteHelper.searchRatingsForRater(rater);

        if(list == null) {
            result  = ("Failed to retrieve ratings. Please try again");
            return;
        }

        result = "";

    }

    private void processSync() {
        if(list == null || list.isEmpty()) {
            setContentView(R.layout.activity_show_no_ratings);
            return;
        }
        setContentView(R.layout.activity_my_ratings);
        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        ListView listView = findViewById(R.id.simpleListView);
        RatingAdapter arrayAdapter = new RatingAdapter(this, list);
        listView.setAdapter(arrayAdapter);
    }


}
