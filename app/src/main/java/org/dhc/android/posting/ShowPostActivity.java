package org.dhc.android.posting;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.text.HtmlCompat;

import org.dhc.android.BaseActivity;
import org.dhc.android.messaging.SendMessageActivity;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.blockchain.Keywords;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionData;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.post.Ratee;
import org.dhc.lite.post.Rating;
import org.dhc.network.Network;
import org.dhc.util.Applications;
import org.dhc.util.Coin;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.android.R;
import org.dhc.util.StringUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class ShowPostActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result = "";
    private List<Rating> list;
    private Ratee ratee;
    private View header;
    private String comment;
    private String rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_post_wait);
        ratee = (Ratee) getIntent().getSerializableExtra("ratee");
        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        findRatings();
    }

    private void findRatings() {
        list = DhcLiteHelper.searchRatings(ratee.getName(), ratee.getTransactionId());

        if(list == null) {
            result  = (getResources().getString(R.string.failed_to_retrieve_ratings));
            return;
        }

        result = "";

    }

    private void processSync() {
        setContentView(R.layout.activity_show_post);

        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        if(list == null) {
            list = new ArrayList<>();
        }
        ListView listView = findViewById(R.id.simpleListView);
        RatingAdapter arrayAdapter = new RatingAdapter(this, list);
        listView.setAdapter(arrayAdapter);

        header =  getLayoutInflater().inflate(R.layout.activity_show_post_header, null);
        listView.addHeaderView(header, null, false);

        TextView textViewPost = header.findViewById(R.id.textViewPost);
        textViewPost.setText(ratee.getName());
        textViewPost.setPaintFlags(textViewPost.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        textViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reply();
            }
        });


        WebView webViewDescription = header.findViewById(R.id.webViewDescription);
        try {
            String data = URLEncoder.encode(ratee.getDescription(), "UTF-8").replaceAll("\\+", "%20");
            webViewDescription.loadData(data, "text/html; charset=utf-8", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
        }


        TextView textViewDate = header.findViewById(R.id.textViewDate);
        textViewDate.setText(new Date(ratee.getTimeStamp()).toString());
        TextView textViewIdentifier = header.findViewById(R.id.textViewIdentifier);
        textViewIdentifier.setText(ratee.getTransactionId());

        Button buttonDeletePost = header.findViewById(R.id.deletePost);
        if(!DhcAddress.getMyDhcAddress().getAddress().equals(ratee.getCreatorDhcAddress())) {
            buttonDeletePost.setVisibility(View.INVISIBLE);
        } else {
            buttonDeletePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deletePost();
                }
            });
        }


        TextView totalRating = header.findViewById(R.id.totalRating);
        totalRating.setText(getResources().getString(R.string.total_rating_is) + " " + ratee.getTotalRating());

        Button buttonAddRating = header.findViewById(R.id.buttonAddRating);
        buttonAddRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load();
                if(!validate()) {
                    return;
                }
                setContentView(R.layout.activity_show_post_wait);
                addRating();
            }
        });

        RadioGroup radioGroup = findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(
            new RadioGroup.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(RadioGroup group, int checkedId) {
                  RadioButton radioButton = findViewById(checkedId);
                  rating = radioButton.getText().toString();
              }
            }
        );

    }

    private void deletePost() {
        new AndroidExecutor() {

            private String deletePostResult;

            @Override
            public void start() {
                deletePostResult = DhcLiteHelper.sendDeleteRateeTransaction(ratee.getName(), ratee.getTransactionId());
            }

            @Override
            public void finish() {
                processDeletePostSync(deletePostResult);
            }
        }.run();
    }

    private void processDeletePostSync(String deletePostResult) {
        if(!StringUtil.isEmpty(deletePostResult)) {
            Toast.makeText(this, deletePostResult, Toast.LENGTH_LONG).show();
        }
    }

    private void addRating() {
        new AndroidExecutor() {

            @Override
            public void start() {

                addRatingAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private boolean validate() {
        if(StringUtil.isEmpty(comment)) {
            Toast.makeText(this, getResources().getString(R.string.comment_cannot_be_empty), Toast.LENGTH_LONG).show();
            return false;
        }
        if(StringUtil.isEmpty(rating)) {
            Toast.makeText(this, getResources().getString(R.string.please_choose_rating), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void load() {
        TextView editTextComment = header.findViewById(R.id.editTextComment);
        comment = StringUtil.trim(editTextComment.getText().toString());
    }

    private TransactionOutput removeOutput(Coin value, Set<TransactionOutput> outputs) {
        logger.info("Will look for output with value {}", value);
        TransactionOutput result = null;
        for(TransactionOutput output: outputs) {
            logger.info("output {}", output);
            if(output.getValue().equals(value)) {
                result = output;
            }
        }
        outputs.remove(result);
        logger.info("result {}", result);
        return result;
    }

    private void addRatingAsync() {
        Keywords accountKeywords = DhcLiteHelper.getKeywords(ratee.getName(), ratee.getTransactionId());

        if(accountKeywords == null || accountKeywords.isEmpty()) {
            result = (getResources().getString(R.string.could_not_retrieve_any_keywords_for) + " " + ratee.getName());
            return;
        }

        Coin amount = Coin.SATOSHI.multiply(2);
        List<Coin> totals = new ArrayList<>();
        totals.add(amount);
        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                totals.add(amount);
            }
        }
        Set<TransactionOutput> originalOutputs = DhcLiteHelper.getTransactionOutputs();
        Coin sum = Coin.ZERO;
        for(TransactionOutput output: originalOutputs) {
            sum = sum.add(output.getValue());
        }

        if(sum.less(amount.multiply(totals.size()))) {
            result = (getResources().getString(R.string.no_inputs_available_for_dhc_address) + " " + DhcAddress.getMyDhcAddress() + ". " + getResources().getString(R.string.please_check_your_balance));
            return;
        }

        Transaction transaction = new Transaction();
        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));

        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();

        Keywords keywords = new Keywords();
        keywords.put("rater", DhcAddress.getMyDhcAddress().toString());
        keywords.put("ratee", ratee.getName());
        keywords.put("transactionId", ratee.getTransactionId());
        keywords.put("rating", rating);

        TransactionOutput output = removeOutput(amount, outputs);
        TransactionData data = new TransactionData(comment);
        transaction = new Transaction();
        transaction.create(CryptoUtil.getDhcAddressFromString(ratee.getName()), Coin.SATOSHI, Coin.SATOSHI, data, keywords, output, Applications.RATING);
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.debug("sent {}", transaction);

        addRating(transaction);

        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                Keywords words = keywords.copy();
                words.remove("rater");

                DhcAddress recipient = CryptoUtil.getDhcAddressFromString(word);
                output = removeOutput(amount, outputs);

                Transaction keyWordTransaction = new Transaction();
                data = new TransactionData(comment);
                keyWordTransaction.create(recipient, Coin.SATOSHI, Coin.SATOSHI, data, words, output, Applications.RATING);
                Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(keyWordTransaction));
                logger.debug("sent {}", keyWordTransaction);
            }
        }

        result = (getResources().getString(R.string.submitted_rating_for) + " " + ratee.getName());
    }

    private void addRating(Transaction transaction) {
        Keywords keywords  = transaction.getKeywords();
        String rate = keywords.get("rating");
        Rating rating = new Rating();
        rating.setRater(DhcAddress.getMyDhcAddress().toString());
        rating.setComment(transaction.getExpiringData().getData());
        rating.setRate(rate);
        rating.setRatee(keywords.get("ratee"));
        rating.setTimeStamp(System.currentTimeMillis());

        String transactionId = keywords.get("transactionId") ;
        rating.setTransactionId(transactionId);
        list.add(0, rating);
    }

    private void reply() {
        Intent intent = new Intent(this, SendMessageActivity.class);
        intent.putExtra("subject", ratee.getName());
        intent.putExtra("recipient", ratee.getCreatorDhcAddress());
        startActivity(intent);
    }


}
