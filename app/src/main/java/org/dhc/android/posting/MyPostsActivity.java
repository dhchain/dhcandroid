package org.dhc.android.posting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.R;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.lite.post.Ratee;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.StringUtil;

import java.util.List;

public class MyPostsActivity extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String result = "";
    private List<Ratee> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            doOnCreate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void doOnCreate() {
        setContentView(R.layout.activity_my_posts_wait);
        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start()  {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        findPosts();
    }

    private void findPosts() {

        list = DhcLiteHelper.getPosts(DhcAddress.getMyDhcAddress());

        if(list == null) {
            result  = ("Failed to retrieve posts. Please try again");
            return;
        }

        result = "";

    }

    private void processSync() {
        setContentView(R.layout.activity_my_posts);
        if(!StringUtil.isEmpty(result)) {
            Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        }
        if(list == null || list.isEmpty()) {
            setContentView(R.layout.activity_show_no_post);
            return;
        }
        ListView listView = findViewById(R.id.simpleListView);
        RateeAdapter arrayAdapter = new RateeAdapter(this, list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Ratee ratee = (Ratee) parent.getItemAtPosition(position);
                showPost(ratee);
            }
        });
    }

    private void showPost(Ratee ratee) {
        Intent intent = new Intent(this, ShowPostActivity.class);
        intent.putExtra("ratee", ratee);
        startActivity(intent);
    }


}
