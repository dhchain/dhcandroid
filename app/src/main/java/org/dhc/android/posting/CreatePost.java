package org.dhc.android.posting;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.dhc.android.BaseActivity;
import org.dhc.android.util.AndroidExecutor;
import org.dhc.android.util.DHCAndroidUtil;
import org.dhc.blockchain.Keywords;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionData;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.lite.DhcLiteHelper;
import org.dhc.network.Network;
import org.dhc.util.Applications;
import org.dhc.util.Coin;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.android.R;
import org.dhc.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CreatePost extends BaseActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();

    private String post;
    private String description;
    private Set<String> words;
    private String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        Button createPostAccount = findViewById(R.id.createPost);
        createPostAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        createPostAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                buttonClicked();

            }
        });
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());

        EditText editTextKeywords = findViewById(R.id.editTextKeywords);
        editTextKeywords.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    hideKeyboard(v);
                    buttonClicked();
                }
                return false;
            }
        });

    }

    private void buttonClicked() {
        load();
        if(!validate()) {
            return;
        }
        Toast.makeText(CreatePost.this, getResources().getString(R.string.wait), Toast.LENGTH_LONG).show();
        setContentView(R.layout.activity_create_post_wait);
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
        process();
    }

    private void process() {
        new AndroidExecutor() {

            @Override
            public void start() {
                processAsync();
            }

            @Override
            public void finish() {
                processSync();
            }
        }.run();
    }

    private void processAsync() {
        if(sendCreateRateeTransaction() != null) {
            result = "Post " + post + " was created successfully with keywords: " + words;
        }
    }

    private void processSync() {
        setContentView(R.layout.activity_create_post_complete);
        TextView resultTextView = findViewById(R.id.resultTextView);
        resultTextView.setText(result);
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        updateStatus(getResources().getString(R.string.network_status) + ": " + DHCAndroidUtil.getNetworkStatus());
    }

    private boolean validate() {
        if (StringUtil.isEmpty(post)) {
            Toast.makeText(this, getResources().getString(R.string.post_cannot_be_empty), Toast.LENGTH_LONG).show();
            return false;
        }
        if(post.length() > Keywords.KEYWORD_MAX_LENGTH) {
            Toast.makeText(this, getResources().getString(R.string.post_cannot_be_longer), Toast.LENGTH_LONG).show();
            return false;
        }

        if (StringUtil.isEmpty(description)) {
            Toast.makeText(this, getResources().getString(R.string.description_cannot_be_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (words.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.keywords_cannot_be_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void load() {
        EditText editTextPost = findViewById(R.id.editTextPost);
        post = editTextPost.getText().toString();
        EditText editTextDescription = findViewById(R.id.editTextDescription);
        description = editTextDescription.getText().toString();
        EditText editTextKeywords = findViewById(R.id.editTextKeywords);
        String keywords = editTextKeywords.getText().toString();
        words = DHCAndroidUtil.getKeywords(keywords);
    }

    private TransactionOutput removeOutput(Coin value, Set<TransactionOutput> outputs) {
        logger.info("Will look for output with value {}", value);
        TransactionOutput result = null;
        for(TransactionOutput output: outputs) {
            logger.info("output {}", output);
            if(output.getValue().equals(value)) {
                result = output;
            }
        }
        outputs.remove(result);
        logger.info("result {}", result);
        return result;
    }

    private Transaction sendCreateRateeTransaction() {
        DhcAddress ratee = CryptoUtil.getDhcAddressFromString(post);
        Coin amount = Coin.SATOSHI.multiply(2); // one satoshi for amount, another satoshi for fee
        List<Coin> totals = new ArrayList<>();
        totals.add(amount);
        for(@SuppressWarnings("unused") String word: words) {
            totals.add(amount);
        }
        Set<TransactionOutput> originalOutputs = DhcLiteHelper.getTransactionOutputs();
        Coin sum = Coin.ZERO;
        for(TransactionOutput output: originalOutputs) {
            logger.info("output {}", output);
            sum = sum.add(output.getValue());
        }

        logger.info("sum={}, amount.multiply(totals.size())={}", sum, amount.multiply(totals.size()));

        if(sum.less(amount.multiply(totals.size()))) {
            result = (getResources().getString(R.string.no_inputs_available_for_dhc_address) + " " + DhcAddress.getMyDhcAddress() + ". " + getResources().getString(R.string.please_check_your_balance));
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));

        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();

        Keywords keywords = new Keywords();
        keywords.put("create", post);
        keywords.put("first", post);
        for(String word: words) {
            keywords.put(word, word);
        }

        TransactionOutput output = removeOutput(amount, outputs);
        TransactionData data = new TransactionData(description);
        transaction = new Transaction();
        transaction.create(ratee, Coin.SATOSHI, Coin.SATOSHI, data, keywords, output, Applications.RATING);
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.debug("sent {}", transaction);

        for(String word: words) {
            Keywords keywordsLoop = keywords.copy();
            keywordsLoop.put("transactionId", transaction.getTransactionId());
            keywordsLoop.put("first", word);
            DhcAddress recipient = CryptoUtil.getDhcAddressFromString(word);
            output = removeOutput(amount, outputs);

            Transaction keyWordTransaction = new Transaction();
            data = new TransactionData(description);
            keyWordTransaction.create(recipient, Coin.SATOSHI, Coin.SATOSHI, data, keywordsLoop, output, Applications.RATING);
            Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(keyWordTransaction));
            logger.debug("sent {}", keyWordTransaction);
        }

        return transaction;
    }
}
