package org.dhc.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import org.dhc.android.tools.ConnectedToNetworkActivity;
import org.dhc.android.util.AndroidConstants;
import org.dhc.android.util.PasswordUtil;
import org.dhc.android.util.StartNetwork;
import org.dhc.network.Network;
import org.dhc.util.Constants;
import org.dhc.util.DhcLogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends StartActivity {

    private static final DhcLogger logger = DhcLogger.getLogger();
    private static final int REQUEST_CODE = 1;
    private Toast connectToast;

    private PasswordUtil passwordUtil = new PasswordUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!Network.getInstance().getMyBucketPeers().isEmpty()) {
            connectedToDhcNetwork();
        }

        String path = getFilesDir().getAbsolutePath();
        Constants.FILES_DIRECTORY = path + "/";
        File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
        if(keyFile.exists()) {
            complete();
            return;
        }

        showSelectKeyFileDialog();

    }

    public void connectedToDhcNetwork() {
        if(connectToast != null) {
            connectToast.cancel();
        }
        Intent intent = new Intent(this, ConnectedToNetworkActivity.class);
        startActivity(intent);
        finish();
    }

    private void showSelectKeyFileDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.key_file))
                .setMessage(getString(R.string.existing_key_file)+"?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        selectKeyFile();
                    }})
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        createNewKeyFile();
                    }}).show();
    }

    private boolean startNetwork(String passphrase) {

        try {
            if (!passwordUtil.loadKeys(passphrase)) {
                logger.error(getResources().getString(R.string.no_load_key_file));
                File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
                if(keyFile.exists()) {
                    Toast.makeText(this, getResources().getString(R.string.wrong_password), Toast.LENGTH_LONG).show();
                }
                return false;
            }
            connectToast = Toast.makeText(this, getResources().getString(R.string.connecting_wait), Toast.LENGTH_LONG);
            connectToast.show();
            logger.debug("DHC starting up");
            StartNetwork.getInstance().start(this);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    private void createNewKeyFile() {
        setContentView(R.layout.activity_main_confirm);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        Button submitButton = findViewById(R.id.button);
        final EditText password = findViewById(R.id.password);
        password.setTransformationMethod(new MyPasswordTransformationMethod());
        final EditText confirmPassword = findViewById(R.id.confirmPassword);
        confirmPassword.setTransformationMethod(new MyPasswordTransformationMethod());

        confirmPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(!password.getText().toString().equals(confirmPassword.getText().toString())) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.reentered_password_does_not_match), Toast.LENGTH_LONG).show();
                        return false;
                    }
                    PasswordUtil passwordUtil = new PasswordUtil();
                    passwordUtil.generateKey(password.getText().toString());
                    doStartNetwork(v, password.getText().toString(), pgsBar);
                }
                return false;
            }
        });

        submitButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!password.getText().toString().equals(confirmPassword.getText().toString())) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.reentered_password_does_not_match), Toast.LENGTH_LONG).show();
                    return;
                }
                PasswordUtil passwordUtil = new PasswordUtil();
                passwordUtil.generateKey(password.getText().toString());
                doStartNetwork(v, password.getText().toString(), pgsBar);
            }
        });
    }

    private boolean doStartNetwork(View v, String password, ProgressBar pgsBar) {
        hideKeyboard(v);
        boolean result = startNetwork(password);
        if(result) {
            pgsBar.setVisibility(View.VISIBLE);
        }
        return result;
    }

    private void selectKeyFile() {
        Intent intent = new Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select key.csv"), REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            Uri selectedFile = data.getData(); //The uri with the location of the file
            File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
            File parentDirectory = keyFile.getParentFile();
            boolean parentDirectoryCreated = false;
            if(parentDirectory != null) {
                parentDirectoryCreated = parentDirectory.mkdirs();
            }
            if(parentDirectoryCreated) {
                logger.debug("Parent directory created: {}", parentDirectory.getAbsolutePath());
            }

            copyFile(selectedFile, keyFile.getAbsolutePath());
        }
        complete();
        Toast.makeText(getApplicationContext(), "Key File was copied to " + getFilesDir().getAbsolutePath(), Toast.LENGTH_LONG).show();
    }


    private void complete() {
        setContentView(R.layout.activity_main);
        final ProgressBar pgsBar = findViewById(R.id.progressBar);
        Button submitButton = findViewById(R.id.button);
        final EditText password = findViewById(R.id.password);
        password.setTransformationMethod(new MyPasswordTransformationMethod());

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(!doStartNetwork(v, password.getText().toString(), pgsBar)) {
                        File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
                        if(!keyFile.exists()) {
                            showPreviousKeyFileDialog();
                        }
                    }

                }
                return false;
            }
        });

        submitButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!doStartNetwork(v, password.getText().toString(), pgsBar)) {
                    File keyFile = new File(Constants.FILES_DIRECTORY + AndroidConstants.KEYS);
                    if(!keyFile.exists()) {
                        showPreviousKeyFileDialog();
                    }
                }
            }
        });
    }

    private void showPreviousKeyFileDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Key File")
                .setMessage("Previously selected key file is invalid. Do you want to use another valid key file?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        selectKeyFile();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        createNewKeyFile();
                    }}).show();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void copyFile(Uri selectedFile, String destination) {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = getContentResolver().openInputStream(selectedFile);
            if(is == null) {
                return;
            }
            os = new FileOutputStream(destination);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(is != null) {
                    is.close();
                }
                if(os != null) {
                    os.close();
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
