package org.dhc.android;

import android.app.Activity;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import org.dhc.android.coins.GetBalanceActivity;
import org.dhc.android.coins.GetMyBalanceActivity;
import org.dhc.android.coins.SendTransactionActivity;
import org.dhc.android.coins.ShowMempoolActivity;
import org.dhc.android.coins.ShowTransactionsActivity;
import org.dhc.android.file.ExportFilesConfig;
import org.dhc.android.file.GetFilesConfig;
import org.dhc.android.file.LogViewerActivity;
import org.dhc.android.file.ShowPeersActivity;
import org.dhc.android.join.JoinTeamActivity;
import org.dhc.android.join.ViewTeamActivity;
import org.dhc.android.messaging.NewMessageNotifier;
import org.dhc.android.messaging.SendMessageActivity;
import org.dhc.android.messaging.ShowMessagesActivity;
import org.dhc.android.posting.CreatePost;
import org.dhc.android.posting.FindPostActivity;
import org.dhc.android.posting.MyPostsActivity;
import org.dhc.android.posting.MyRatingsActivity;
import org.dhc.android.tools.ChangePasswordActivity;
import org.dhc.android.tools.ConnectedToNetworkActivity;
import org.dhc.android.tools.UpdateAppActivity;
import org.dhc.android.util.DHCAndroidUtil;


public class MenuHandler {

    public void handleChangePassword(Activity activity) {
        Intent intent = new Intent(activity, ChangePasswordActivity.class);
        activity.startActivity(intent);
    }

    public void handleUpdateApp(Activity activity) {
        Intent intent = new Intent(activity, UpdateAppActivity.class);
        activity.startActivity(intent);
    }

    public void handleGetFilesConfig(Activity activity) {
        Intent intent = new Intent(activity, GetFilesConfig.class);
        activity.startActivity(intent);
    }

    public void handleExportFilesConfig(Activity activity) {
        Intent intent = new Intent(activity, ExportFilesConfig.class);
        activity.startActivity(intent);
    }

    public void handleGetMyBalance(Activity activity) {
        Intent intent = new Intent(activity, GetMyBalanceActivity.class);
        activity.startActivity(intent);
    }

    public void handleGetBalance(Activity activity) {
        Intent intent = new Intent(activity, GetBalanceActivity.class);
        activity.startActivity(intent);
    }

    public void handleViewLog(Activity activity) {
        Intent intent = new Intent(activity, LogViewerActivity.class);
        activity.startActivity(intent);
    }

    public void handleShowPeers(Activity activity) {
        Intent intent = new Intent(activity, ShowPeersActivity.class);
        activity.startActivity(intent);
    }

    public void handleShowTransactions(Activity activity) {
        Intent intent = new Intent(activity, ShowTransactionsActivity.class);
        activity.startActivity(intent);
    }

    public void handleSendTransaction(Activity activity) {
        Intent intent = new Intent(activity, SendTransactionActivity.class);
        activity.startActivity(intent);
    }

    public void handleShowAddress(Activity activity) {
        Intent intent = new Intent(activity, ConnectedToNetworkActivity.class);
        activity.startActivity(intent);
    }

    public void handleShowMempool(Activity activity) {
        Intent intent = new Intent(activity, ShowMempoolActivity.class);
        activity.startActivity(intent);
    }

    public void handleViewTeam(Activity activity) {
        Intent intent = new Intent(activity, ViewTeamActivity.class);
        activity.startActivity(intent);
    }

    public void handleJoinTeam(Activity activity) {
        Intent intent = new Intent(activity, JoinTeamActivity.class);
        activity.startActivity(intent);
    }

    public void handleExit(Activity activity) {
        Intent service = new Intent(activity, NewMessageNotifier.class);
        service.setAction(DHCAndroidUtil.STOP);
        ContextCompat.startForegroundService(activity, service);
    }

    public void handleSendSecureMessage(Activity activity) {
        Intent intent = new Intent(activity, SendMessageActivity.class);
        activity.startActivity(intent);
    }

    public void handleShowMessages(Activity activity) {
        Intent intent = new Intent(activity, ShowMessagesActivity.class);
        activity.startActivity(intent);
    }

    public void handleCreatePost(Activity activity) {
        Intent intent = new Intent(activity, CreatePost.class);
        activity.startActivity(intent);
    }

    public void handleFindPost(Activity activity) {
        Intent intent = new Intent(activity, FindPostActivity.class);
        activity.startActivity(intent);
    }

    public void handleMyRatings(Activity activity) {
        Intent intent = new Intent(activity, MyRatingsActivity.class);
        activity.startActivity(intent);
    }

    public void handleMyPosts(Activity activity) {
        Intent intent = new Intent(activity, MyPostsActivity.class);
        activity.startActivity(intent);
    }

}
