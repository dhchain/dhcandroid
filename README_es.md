La aplicación DHC Client es una aplicación de Android que permite a los usuarios utilizar las funciones siguientes para interactuar con la red Distributed Hash Chain.

Puede leer más sobre la red Distributed Hash Chain revisando el enlace a continuación:

https://github.com/dhchain/dhc

<a href="http://download.dhcne.org/drive/dhc-android.apk" tagret="_blank" download="dhc-android.apk"> Descargar </a>

La funcionalidad de la aplicación cliente DHC incluye los siguientes menús principales y submenús:

* Menú Archivo: permite a los usuarios importar, exportar y copiar archivos; Además, pueden ver los registros del sistema y mostrar a los compañeros
* Menú de herramientas: permite a los usuarios cambiar su contraseña, mostrar, copiar, compartir su dirección DHC.
* Monedas: permite a los usuarios ver sus saldos, enviar monedas, mostrar transacciones procesadas, mostrar transacciones de peding en Mempool
* Promocionar: los usuarios pueden unirse a nuestro equipo de promoción para donar monedas y aumentar el uso de DHC

La aplicación DHC Client no requiere la descarga de blockchain y se puede instalar e iniciar rápidamente; Lo más importante es que la aplicación DHC Client utiliza menos recursos en su teléfono.

<b> Instalación rápida: </b>

1. Ejecute los pasos de instalación del 1 al 6 a continuación, ignorando todo lo relacionado con el archivo key.csv.
2. Ingrese una frase de contraseña y, lo más importante, recuérdela. & nbsp; & nbsp; & nbsp; & nbsp; nota: nadie puede restaurarlo por usted.
3. Listo, comienza a enviar monedas y únete a un equipo de promoción.

<b> Instrucciones de instalación: </b>

1. Vaya al directorio de descargas de su teléfono (ya sea interno del teléfono o bien SD).
 
<BR />


   ![descargar](images/es/android1.png)
 
<BR />


2. Haga clic en el archivo <a href="https://drive.google.com/u/6/uc?export=download&confirm=E_HN&id=17kEXjcbXH2MvBChmt1aswE37gORqOQBY" tagret="_blank" download="dhc-android.apk"> dhc-android.apk </a> en este directorio (es decir, directorio bitbucket dhcandroid) y haga clic en el enlace Ver sin procesar. Descárguelo en su directorio de descargas desde el paso uno anterior.
3. Si ya tiene el archivo key.csv con claves existentes, simplemente cópielo en el directorio de descargas de su teléfono; de lo contrario, creará una nueva clave.
4. Haga clic en el archivo dhc-android.apk ubicado en el directorio de descargas de su teléfono desde el paso uno anterior.
 
<BR />


   ![archivo apk](images/es/android2.png)
 
<BR />


5. Haga clic en Instalar
 
<BR />


   ![instalar](images/es/android3.png)
 
<BR />


6. Una vez instalado, seleccione Abrir. Importante: si tiene un archivo key.csv, cópielo en el directorio de descargas antes de seleccionar Abrir.

<b> Iniciar sesión en la aplicación </b>
 
<BR />


   ![newLogin](images/es/android2a.png)
 
<BR />


Usuarios por primera vez, presione Cancelar; de lo contrario, presione Aceptar si el archivo key.csv se copió al directorio de descargas de su teléfono.
 
<BR />


   ![archivo apk](images/es/android4b.png)
 
<BR />


Si tiene un archivo key.csv y una frase de contraseña, le pedirá la frase de contraseña existente. La exhibición de ejemplo a continuación es el mensaje cuando un usuario copió su archivo keys.csv existente en el directorio de descargas y luego inició la aplicación.
 
<BR />


   ![iniciar sesión](images/es/android4a.png)
 
<BR />


Una vez que haya iniciado sesión, mostrará su dirección.

Para acceder al menú principal, seleccione la elipsis o el menú de puntos ubicado en la esquina superior derecha. Esto mostrará el menú principal.
 
<BR />


  ![MainMenu](images/es/android5.png)
 
<BR />


<b> Submenú Archivo: </b> permite a los usuarios importar, exportar y copiar archivos; Además, pueden ver los registros del sistema y mostrar a los compañeros

* Importar archivo: seleccione un archivo para importar (vea la exhibición a continuación)
* Exportar archivo: seleccione un archivo para exportar (vea la exhibición a continuación)
* Ver registro: esto se usa para identificar cualquier problema con la aplicación
* Shop Peers: esta lista de pares conectados a la misma instancia a la que está conectado
 
<BR />


 ![MainMenu](images/es/android8.png)
 
<BR />


 Importar archivo:
 
<BR />


 ![importar](images/es/android6.png)
 
<BR />


 Exportar archivo:
 
<BR />


 ![exportar](images/es/android7.png)
 
<BR />


<b> Submenú de herramientas: </b> permite al usuario cambiar su contraseña, obtener saldos y enviar monedas.

* Cambiar contraseña: permite al usuario cambiar su contraseña
* Mostrar dirección: permite al usuario ver su dirección actual
 
<BR />


![MainMenu](images/es/android9.png)
 
<BR />


 Cambia la contraseña:
 
<BR />

 ![contraseña](images/es/android10.png)
 
<BR />

 Mostrar dirección:
 
<BR />

![MostrarDirección](images/es/android14.png)