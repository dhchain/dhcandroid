En español: <a href="README_es.md">README_es.md</a>

DHC Client App is an Android application, which allows users to use functionality below to interact with the Distributed Hash Chain network.

You can read more about the Distributed Hash Chain network by reviewing the link below:

https://github.com/dhchain/dhc

<a href="http://download.dhcne.org/drive/dhc-android.apk" tagret="_blank" download="dhc-android.apk">Download</a>

DHC Client App functionality includes the following main and sub menus:

* File menu: allows users to import, export, and copy files; in addition, they can view system logs and show peers
* Tools menu: allows users to change their password, show, copy, share their DHC address.
* Coins: allows users to view their balances, send coins, show processed transactions, show peding transactions in Mempool
* Promote: users can join our promotion team to donate coins and increase usage of DHC

DHC Client App does not require downloading of blockchain and can be installed and started quickly; most importantly, DHC Client App uses less resources on your phone. 

<b>Quick Installation:</b>

1. Execute installation steps 1 - 6 below, ignoring anything about key.csv file.
2. Enter a passphrase, and most important, remember it. &nbsp;&nbsp;&nbsp;&nbsp; note: no one can restore it for you.
3. Done, start sending coins and joining a promotion team.

<b>Installation instructions:</b>

1. Go to your phone's Download directory(either phone internal or SD ok). 

<BR />

   ![download](images/android1.jpg) 

<BR />

2. Click on the  <a href="https://drive.google.com/u/6/uc?export=download&confirm=E_HN&id=17kEXjcbXH2MvBChmt1aswE37gORqOQBY" tagret="_blank" download="dhc-android.apk">dhc-android.apk</a> file in the this directory(i.e. bitbucket dhcandroid directory) and download it to your Download directory from step one above.
3. If you already have key.csv file with existing keys, just copy it to your phone's Download directory, otherwise, it will create new key. 
4. Click on the dhc-android.apk file located in your phone's Download directory from step one above. 

<BR />

   ![apkFile](images/android2.jpg)

<BR />

5. Click Install

<BR />

   ![install](images/android3.png)
   
<BR />  

6. Once installed, select Open. Important: if you have a key.csv file, copy it to the Download directory before you select Open.

<b>Logging in to App</b>

<BR />

   ![newLogin](images/android2a.png)

<BR />

First time users, press Cancel; otherwise, press Ok if the key.csv was copied to your phone's Download directory.

<BR />

   ![apkFile](images/android4b.png)

<BR />

If you do have a key.csv file and passphrase, it will prompt you for the existing passphrase. The example exhibit below is the prompt when a user  copied their existing keys.csv file to the Download directory and then started the app.

<BR />

   ![login](images/android4a.png)

<BR />

Once logged in, it will display your address.

To access the main menu, select the ellipsis or dot menu located on the upper right corner. This will display the main menu.

<BR />

  ![mainMenu](images/android5.png)

<BR />

<b>File sub menu:</b> allows users to import, export, and copy files; in addition, they can view system logs and show peers

* Import File: select a file to import(see exhibit below)
* Export File: select a file to export(see exhibit below)
* View Log: this is used to identify any issues with app
* Shop Peers: this list peers connected to the same instance you are connected

<BR />

 ![mainMenu](images/android8.png)

<BR />

 Import File:
 
<BR />

 ![import](images/android6.png)

<BR />

 Export File:

<BR />

 ![export](images/android7.png)

<BR />

<b>Tools sub menu:</b> allows user to change their password, get balances, and send Coins.

* Change Password: allows user to change their password
* Show Address: allows user to see their current address

<BR />
  

![mainMenu](images/android9.png)

<BR />  
  

 Change Password:
   
<BR />   

 ![password](images/android10.png) 
   
<BR />  

Show Address:


<BR />


![showAddress](images/android14.png)




